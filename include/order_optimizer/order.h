#ifndef ORDER_OPTIMIZER_ORDER_H_
#define ORDER_OPTIMIZER_ORDER_H_

#include <stdlib.h>

#include <string>
#include <vector>
#include <iostream>

#include "order_optimizer/product.h"
#include "order_optimizer/location.h"

namespace order_optimizer {

    class Order {
        public:
        Order();
        explicit Order(const uint32_t id);
        Order(const uint32_t id, std::vector<Product*> products);

        void SetId(const uint32_t id);
        uint32_t GetId();
        void SetLocation(const Location l);
        Location GetLocation();
        void SetProducts(std::vector<Product*> products);
        std::vector<Product*> GetProducts();

        void AddProduct(Product* product);
        Product* GetProduct(uint32_t index);

        std::string ToString();

        private:
        uint32_t id_;
        Location location_;
        std::vector<Product*> products_;
    };

}   // namespace order_optimizer

#endif  //ORDER_OPTIMIZER_ORDER_H_