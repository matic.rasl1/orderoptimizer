#ifndef ORDER_OPTIMIZER_COORDINATE_H_
#define ORDER_OPTIMIZER_COORDINATE_H_

#include <stdlib.h>

#include <string>
#include <cmath>

namespace order_optimizer {

    class Location {
        public:
        Location();
        Location(float x, float y);

        void SetX(const float x);
        float GetX();
        void SetY(const float y);
        float GetY();
        float Distance(Location l);

        std::string ToString();

        private:
        float x_;
        float y_;
    };

}   // namespace order_optimizer

#endif  // ORDER_OPTIMIZER_COORDINATE_H_