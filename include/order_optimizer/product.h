#ifndef ORDER_OPTIMIZER_PRODUCT_H_
#define ORDER_OPTIMIZER_PRODUCT_H_

#include <stdlib.h>

#include <string>
#include <vector>
#include <iostream>

#include "order_optimizer/part.h"

namespace order_optimizer {

    class Product {
        public:
        Product();
        explicit Product(const uint32_t id, std::string name);
        Product(const uint32_t id, std::string n, std::vector<Part*> parts);

        void SetId(const uint32_t id);
        int GetId();
        void SetName(std::string name);
        std::string GetName();
        void SetParts(std::vector<Part*> parts);
        std::vector<Part*> GetParts();
        uint32_t GetPartsSize();

        void AddPart(Part* part);
        Part* GetPart(uint32_t index);
        uint32_t GetPartCount(Part* part);

        std::string ToString();

        private:
        uint32_t id_;
        std::string name_;
        std::vector<Part*> parts_;
    };

}   // namespace order_optimizer

#endif  //ORDER_OPTIMIZER_PRODUCT_H_