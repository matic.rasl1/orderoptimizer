#ifndef ORDER_OPTIMIZER_PATH_FINDER_H_
#define ORDER_OPTIMIZER_PATH_FINDER_H_

#include <stdlib.h>

#include <string>
#include <map>
#include <vector>

#include "order_optimizer/location.h"
#include "order_optimizer/order.h"
#include "order_optimizer/product.h"
#include "order_optimizer/part.h"

namespace order_optimizer {

    class PathFinder {
        public:

        static float GetShortestPath(Location start, const Location end, std::map<std::string, Part*> required_parts, std::vector<Part*> &path);
        static std::string GeneratePathPrint(Order *order, std::string order_description, std::vector<Part*> shortest_path);

        private:
        PathFinder();
        
        static float GetPathLength(Location start, Location end, std::vector<Part*> parts, std::vector<Part*> &path, bool* &selectable_parts, uint32_t depth);
    };

}   // namespace order_optimizer

#endif  // ORDER_OPTIMIZER_PATH_FINDER_H_