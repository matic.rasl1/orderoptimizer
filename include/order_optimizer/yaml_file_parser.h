#ifndef ORDER_OPTIMIZER_YAML_FILE_PARSER_H_
#define ORDER_OPTIMIZER_YAML_FILE_PARSER_H_

#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <thread>
#include <functional>

#include "order_optimizer/order.h"
#include "order_optimizer/product.h"
#include "order_optimizer/part.h"
#include "order_optimizer/location.h"

namespace order_optimizer {

class YamlFileParser {
    public:

    static bool ParseConfigurationFile(std::string path, std::map<uint32_t, Product*> &products, std::map<std::string, Part*> &parts);

    static void ParseOrders(std::string path, std::map<uint32_t, Product*> products, std::map<uint32_t, Order*> &all_orders_map);

    static void ParseOrdersFile(std::string path, std::map<uint32_t, Product*> products, uint32_t file_index);

    private:

    static std::vector< std::vector<Order*> > orders_;

    YamlFileParser();

    enum DataType{kOrder=0, kProduct=1, kPart=2};
    
    static bool IsNewProductLine(std::string &line);
    static bool ProductExists(uint32_t id, std::map<uint32_t, Product*> *products);
    static uint32_t IdFromLine(std::string &line, const unsigned short kStartIndex);
    static std::string NameFromLine(std::string &line, const unsigned short kStartIndex);
    static bool IsNewPartLine(std::string line);
    static bool PartExists(std::string part_name, std::map<std::string, Part*> &parts);
    static float LocationFromLine(std::string &line, const unsigned short kStartIndex);
    static bool IsNewOrderLine(std::string &line);
    static void ClearOrders();

};

}   // namespace order_optimizer

#endif  //ORDER_OPTIMIZER_YAML_FILE_PARSER_H_