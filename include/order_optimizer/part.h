#ifndef ORDER_OPTIMIZER_PART_H_
#define ORDER_OPTIMIZER_PART_H_

#include <stdlib.h>

#include <string>

#include "order_optimizer/location.h"

namespace order_optimizer {

    class Part {
        public:
        Part();
        explicit Part(std::string name);
        Part(std::string name, float x, float y);

        void SetName(std::string name);
        std::string GetName();
        void SetLocation(const Location l);
        Location GetLocation();

        std::string ToString();

        private:
        std::string name_;
        Location location_;
    };

}   // namespace order_optimizer

#endif  // ORDER_OPTIMIZER_PART_H_