#include "order_optimizer/yaml_file_parser.h"

namespace order_optimizer {

std::vector< std::vector<Order*> > YamlFileParser::orders_ = std::vector< std::vector<Order*> >();

YamlFileParser::YamlFileParser() {}

// Parsing the configuration file on node startup
bool YamlFileParser::ParseConfigurationFile(std::string path, std::map<uint32_t, Product*> &products, std::map<std::string, Part*> &parts) {
    std::ifstream file;
    file.open(path, std::ios::in);

    Product *product;
    Part *part;
    Location location = Location();

    bool skipping_to_next_product = true;
    uint32_t new_product_id;
    
    std::string line;
    if(file.is_open()) {
        // Iterating through the file
        while(std::getline(file, line)) {
            if(IsNewProductLine(line)) {
                skipping_to_next_product = false;

                new_product_id = IdFromLine(line, 6);
                
                if(!ProductExists(new_product_id, &products)) {
                    // Product does not yet exist

                    // Add new product with id
                    product = new Product();
                    product->SetId(new_product_id);

                    // Add product name
                    std::getline(file, line);
                    product->SetName(NameFromLine(line, 12));
                    
                    // Skip one line
                    std::getline(file, line);

                    products[new_product_id] = product;
                }
                else {
                    // Skip to the next product
                    skipping_to_next_product = true;
                }
            }
            else if(IsNewPartLine(line) && !skipping_to_next_product) {
                std::string new_part_name = NameFromLine(line, 11);
                if(!PartExists(new_part_name, parts)) {
                    // Part does not yet exist

                    // Add new part with name
                    part = new Part();
                    part->SetName(new_part_name);

                    // Add cx
                    std::getline(file, line);
                    location.SetX(LocationFromLine(line, 8));

                    // Add cy
                    std::getline(file, line);
                    location.SetY(LocationFromLine(line, 8));

                    part->SetLocation(location);

                    parts[new_part_name] = part;
                }
                else {
                    // Skip 2 lines
                    std::getline(file, line);
                    std::getline(file, line);
                }
                // Add the part to the current product
                products[new_product_id]->AddPart(parts[new_part_name]);
            }
        }

        file.close();

        return true;
    }
    else {
        return false;
    }
}

// Parse orders
void YamlFileParser::ParseOrders(std::string path, std::map<uint32_t, Product*> products, std::map<uint32_t, Order*> &all_orders_map) {
    auto dir_iter = std::filesystem::directory_iterator(path);
    std::vector<std::thread> threads;
    std::vector<std::string> filenames;

    ClearOrders();

    // Listing all filenames
    for(auto& file_name: std::filesystem::directory_iterator(path)) {
        filenames.push_back(file_name.path().string());
        orders_.push_back(std::vector<Order*>());
    }

    // Multithreading
    for(uint16_t i = 0, n = filenames.size(); i < n; i++) {
        threads.push_back(std::thread(ParseOrdersFile, filenames[i], products, i));
    }

    // Joining threads
    for(uint16_t i = 0; i < filenames.size(); i++) {
        threads[i].join();
    }

    // Merging parsed data together
    for(uint32_t i = 0, n = orders_.size(); i < n; i++) {
        for(uint32_t j = 0, m = orders_[i].size(); j < m; j++) {
            all_orders_map[orders_[i][j]->GetId()] = orders_[i][j];
        }
    }
}

// Parse orders files 
void YamlFileParser::ParseOrdersFile(std::string path, std::map<uint32_t, Product*> products, uint32_t file_index) {
    std::ifstream file;
    file.open(path, std::ios::in);

    int32_t product_id;
    Location location = Location();

    std::string line;
    if(file.is_open()) {
        while(std::getline(file, line)) {
            if(IsNewOrderLine(line)) {
                orders_[file_index].push_back(new Order(IdFromLine(line, 9)));
                // Get cx
                std::getline(file, line);
                location.SetX(LocationFromLine(line, 6));
                // Get cy
                std::getline(file, line);
                location.SetY(LocationFromLine(line, 6));

                orders_[file_index].back()->SetLocation(location);
                
                // Skip one line
                std::getline(file, line);
            }
            else {
                // New product in the order
                product_id = IdFromLine(line, 4);
                orders_[file_index].back()->AddProduct(products[product_id]);
            }
        }
    }
}

bool YamlFileParser::IsNewProductLine(std::string &line) {
    return line[0] == '-';
}

bool YamlFileParser::ProductExists(uint32_t id, std::map<uint32_t, Product*> *products) {
    return products->find(id) != products->end();
}

uint32_t YamlFileParser::IdFromLine(std::string &line, const unsigned short kStartIndex) {
    const unsigned short kNumSize = line.length();
    
    uint32_t num = 0;
    for(unsigned short i = kStartIndex; i < kNumSize; i++) {
        num *= 10;
        num += line[i] - 48;
    }

    return num;
}

std::string YamlFileParser::NameFromLine(std::string &line, const unsigned short kStartIndex) {
    std::string name = "";

    for(unsigned int i = kStartIndex; line[i] != '\"' && i < line.length(); i++) {
        name += line[i];
    }

    return name;
}

bool YamlFileParser::IsNewPartLine(std::string line) {
    return line[2] == '-';
}

bool YamlFileParser::PartExists(std::string part_name, std::map<std::string, Part*> &parts) {
    return parts.find(part_name) != parts.end();
}

float YamlFileParser::LocationFromLine(std::string &line, const unsigned short kStartIndex) {
    float c = 0.0f;
    float decimal = 0.0f;
    float decimal_mul = 1.0f;

    unsigned short i = kStartIndex;

    while(line[i] != '.') {
        c *= 10.0;
        c += line[i++] - 48;
    }
    i++;
    while(i < line.length()) {
        decimal_mul /= 10;
        decimal += (line[i++] - 48) * decimal_mul;
    }

    return c + decimal;
}

bool YamlFileParser::IsNewOrderLine(std::string &line) {
    return line[0] == '-';
}

void YamlFileParser::ClearOrders() {
    for(uint32_t i = 0; i < orders_.size(); i++) {
        for(uint32_t j = 0; j < orders_[i].size(); j++) {
            delete orders_[i][j];
        }
        orders_[i].clear();
    }
    orders_.clear();
}

}   // namespace order_optimizer