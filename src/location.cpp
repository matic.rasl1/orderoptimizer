#include "order_optimizer/location.h"

namespace order_optimizer {

Location::Location() : x_(0.0), y_(0.0) {}

Location::Location(float x, float y) : x_(x), y_(y) {}

void Location::SetX(const float x) {
    this->x_ = x;
}

float Location::GetX() {
    return this->x_;
}

void Location::SetY(const float y) {
    this->y_ = y;
}

float Location::GetY() {
    return this->y_;
}

float Location::Distance(Location l) {
    return sqrt(pow(this->x_ - l.x_, 2) + pow(this->y_ - l.y_, 2));
}

std::string Location::ToString() {
    std::string out_string = "x: " + std::to_string(this->x_) + ", y: " + std::to_string(this->y_);
    return out_string;
}

}   // namespace order_optimizer