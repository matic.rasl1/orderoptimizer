#include "order_optimizer/part.h"

namespace order_optimizer {

Part::Part() : name_(""), location_(Location()) {}

Part::Part(std::string name) : name_(name), location_(Location()) {}

Part::Part(std::string name, float x, float y) : name_(name), location_(Location(x, y)) {}

void Part::SetName(std::string name) {
    this->name_ = name;
}

std::string Part::GetName() {
    return this->name_;
}

void Part::SetLocation(const Location l) {
    this->location_ = l;
}

Location Part::GetLocation() {
    return this->location_;
}

std::string Part::ToString() {
    std::string out_string = "";
    out_string += "\t\tname: " + this->name_ + "\t" + this->location_.ToString() + "\n";
    return out_string;
}

}   // namespace order_optimizer