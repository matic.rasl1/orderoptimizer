#include "order_optimizer/order.h"

namespace order_optimizer {

Order::Order() : id_(0), products_(std::vector<Product*>()) {}

Order::Order(const uint32_t id) : id_(id), products_(std::vector<Product*>()) {}

Order::Order(const uint32_t id, std::vector<Product*> products) : id_(id), products_(products) {}

void Order::SetId(const uint32_t id) {
    this->id_ = id;
}

uint32_t Order::GetId() {
    return this->id_;
}

void Order::SetLocation(const Location l) {
    this->location_ = l;
}

Location Order::GetLocation() {
    return this->location_;
}

void Order::SetProducts(std::vector<Product*> products) {
    this->products_ = products;
}

std::vector<Product*> Order::GetProducts() {
    return this->products_;
}

void Order::AddProduct(Product* product) {
    this->products_.push_back(product);
}

Product* Order::GetProduct(uint32_t index) {
    if(index >= this->products_.size())
        return nullptr;

    return this->products_[index];
}

std::string Order::ToString() {
    std::string out_string = "Order id: " + std::to_string(this->id_) + this->location_.ToString() + "\n";

    for(uint32_t i = 0, n = this->products_.size(); i < n; i++) {
        out_string += this->products_[i]->ToString();
    }

    return out_string;
}

}   // namespace order_optimizer