#include <stdio.h>

#include <cstdio>
#include <chrono>
#include <memory>
#include <string>
#include <vector>
#include <map>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <order_optimizer/msg/order.hpp>

#include "order_optimizer/order.h"
#include "order_optimizer/product.h"
#include "order_optimizer/part.h"
#include "order_optimizer/yaml_file_parser.h"
#include "order_optimizer/location.h"
#include "order_optimizer/path_finder.h"

using std::placeholders::_1;

namespace order_optimizer {

class OrderOptimizer : public rclcpp::Node {
    public:
    OrderOptimizer() : Node("order_optimizer_node") {
        // Setup of subscribers
        current_position_subscription_ = this->create_subscription<geometry_msgs::msg::PoseStamped>("currentPosition", 10, std::bind(&OrderOptimizer::CurrentPositionCallback, this, _1));
        next_order_subscription_ = this->create_subscription<order_optimizer::msg::Order>("nextOrder", 10, std::bind(&OrderOptimizer::NextOrderCallback, this, _1));

        marker_publisher_ = this->create_publisher<visualization_msgs::msg::Marker>("order_path", 10);

        // Parsing of the configuration file (performed only on node startup)
        if(!YamlFileParser::ParseConfigurationFile(this->path_ + "configuration/products.yaml", this->products_, this->parts_)) {
            std::cout << "Could not locate the configuration file. Are you sure the provided path is correct?" << std::endl;
            return;
        }

        amr_pos_ = Location();
    }
    
    static std::string path_;

    private:
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr current_position_subscription_;
    rclcpp::Subscription<order_optimizer::msg::Order>::SharedPtr next_order_subscription_;
    rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr marker_publisher_;

    std::map<uint32_t, Order*> orders_;
    std::map<uint32_t, Product*> products_;
    std::map<std::string, Part*> parts_;

    Location amr_pos_;

    // nextOrder handler
    void NextOrderCallback(const order_optimizer::msg::Order::SharedPtr msg) {
        // Files in orders must be parsed every time this function is called
        YamlFileParser::ParseOrders(this->path_ + "/orders/", products_, orders_);

        // Listing all products and necessary parts
        uint32_t order_id = msg->order_id;
        if(orders_.find(order_id) == orders_.end()) {
            std::cout << "Invalid order id: " << std::to_string(order_id) << std::endl;
            return;
        }
        Order *order = orders_[order_id];
        std::map<std::string, Part*> required_parts = GetRequiredParts(order);

        // Calculating the shortest path
        std::vector<Part*> shortest_path;
        float shortest_path_length = PathFinder::GetShortestPath(amr_pos_, order->GetLocation(), required_parts, shortest_path);

        // Printing the shortest path
        std::cout << PathFinder::GeneratePathPrint(order, msg->description, shortest_path);

        // Visualization of the parts and the robot
        Visualize(required_parts, order);

        // Setting the new location for the AMR
        amr_pos_ = orders_[order_id]->GetLocation();
    }

    // currentPosition handler
    void CurrentPositionCallback(const geometry_msgs::msg::PoseStamped::SharedPtr msg) {
        amr_pos_.SetX(msg->pose.position.x);
        amr_pos_.SetY(msg->pose.position.y);
    }

    // Returns map of required parts for the order
    std::map<std::string, Part*> GetRequiredParts(Order *order) {
        std::map<std::string, Part*> required_parts;
        std::vector<Product*> order_products = order->GetProducts();
        std::vector<Part*> product_parts;

        for(uint16_t i = 0, n = order_products.size(); i < n; i++) {
            product_parts = order_products[i]->GetParts();
            for(uint16_t j = 0, m = product_parts.size(); j < m; j++) {
                required_parts[product_parts[j]->GetName()] = product_parts[j];
            }
        }

        return required_parts;
    }

    // Visualize the path using visualization_msgs::msg::Marker
    void Visualize(std::map<std::string, Part*> parts, Order *order) {
        visualization_msgs::msg::Marker marker;

        marker.header.frame_id = "/map";

        marker.action = visualization_msgs::msg::Marker::ADD;
        marker.pose.position.z = 0;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;

        marker.scale.x = 10.0;
        marker.scale.y = 10.0;
        marker.scale.z = 10.0;

        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;

        marker.lifetime = rclcpp::Duration(1000, 0);

        marker.type = visualization_msgs::msg::Marker::CUBE;
        marker.id = 0;
        marker.pose.position.x = amr_pos_.GetX();
        marker.pose.position.y = amr_pos_.GetY();
        
        marker_publisher_->publish(marker);

        uint32_t index = 0;

        marker.type = visualization_msgs::msg::Marker::CYLINDER;

        for(auto it = parts.begin(); it != parts.end(); it++) {
            Part* part = it->second;

            marker.id = ++index;
            marker.pose.position.x = part->GetLocation().GetX();
            marker.pose.position.y = part->GetLocation().GetY();

            marker_publisher_->publish(marker);
        }

        marker.type = visualization_msgs::msg::Marker::CUBE;
        marker.id = ++index;
        marker.pose.position.x = order->GetLocation().GetX();
        marker.pose.position.y = order->GetLocation().GetY();
        marker.color.r = 1.0f;
        marker.color.g = 0.0f;

        marker_publisher_->publish(marker);
    }

};

std::string OrderOptimizer::path_ = "";

}   // namespace order_optimizer

int main(int argc, char ** argv)
{
    if(argc < 2) {
        std::cout << "file_path must be specified!" << std::endl;
        return 0;
    }

    std::string path = argv[1];
    order_optimizer::OrderOptimizer::path_ = path;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<order_optimizer::OrderOptimizer>());
    rclcpp::shutdown();
    return 0;
}
