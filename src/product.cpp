#include "order_optimizer/product.h"

namespace order_optimizer {

Product::Product() : id_(0), parts_(std::vector<Part*>()) {}

Product::Product(const uint32_t id, std::string name) : id_(id), name_(name), parts_(std::vector<Part*>()) {}

Product::Product(const uint32_t id, std::string name, std::vector<Part*> parts) : id_(id), name_(name), parts_(parts) {}

void Product::SetId(const uint32_t id) {
    this->id_ = id;
}

int Product::GetId() {
    return this->id_;
}

void Product::SetName(std::string name) {
    this->name_ = name;
}

std::string Product::GetName() {
    return this->name_;
}

void Product::SetParts(std::vector<Part*> parts) {
    this->parts_ = parts;
}

std::vector<Part*> Product::GetParts() {
    return this->parts_;
}

uint32_t Product::GetPartsSize() {
    return this->parts_.size();
}

void Product::AddPart(Part* part) {
    this->parts_.push_back(part);
}

Part* Product::GetPart(uint32_t index) {
    if(index >= this->parts_.size())
        return nullptr;

    return this->parts_[index];
}

uint32_t Product::GetPartCount(Part* part) {
    uint32_t count = 0;

    for(uint32_t i = 0; i < this->parts_.size(); i++) {
        if(this->parts_[i] == part) {
            ++count;
        }
    }

    return count;
}

std::string Product::ToString() {
    std::string out_string = "";
    out_string += "\tid: " + std::to_string(this->id_) + "\n";
    out_string += "\tname: " + this->name_ + "\n";
    out_string += "\tparts: " + std::to_string(this->parts_.size()) + '\n';
    for(uint32_t i = 0; i < this->parts_.size(); i++) {
        out_string += parts_[i]->ToString();
    }
    return out_string;
}

}   // namespace order_optimizer