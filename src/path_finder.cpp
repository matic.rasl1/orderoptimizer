#include "order_optimizer/path_finder.h"

namespace order_optimizer {

float PathFinder::GetShortestPath(Location start, const Location end, std::map<std::string, Part*> required_parts, std::vector<Part*> &path) {
    std::vector<Part*> parts_vector;
    for(auto it = required_parts.begin(); it != required_parts.end(); it++) {
        parts_vector.push_back(it->second);
        //std::cout << it->second->ToString();
    }

    bool* selectable_parts = new bool[required_parts.size()];
    for(uint32_t i = 0; i < required_parts.size(); i++) {
        selectable_parts[i] = true;
    }
    
    float shortest_path = GetPathLength(start, end, parts_vector, path, selectable_parts, 0);

    return shortest_path;
}

// Recursivelly called function for calculating the shortest path by brute force
float PathFinder::GetPathLength(Location start, Location end, std::vector<Part*> parts, std::vector<Part*> &path, bool* &selectable_parts, uint32_t depth) {
    float shortest_path_size = INFINITY;
    std::vector<Part*> shortest_path;
    std::vector<Part*> original_path = path;
    float temp_path_size;

    if(depth == 0) {
        // Connecting parts to the start location
        for(uint32_t i = 0; i < parts.size(); i++) {
            selectable_parts[i] = false;
            path.push_back(parts[i]);

            temp_path_size = GetPathLength(start, end, parts, path, selectable_parts, depth + 1) + start.Distance(parts[i]->GetLocation());
            if(temp_path_size < shortest_path_size) {
                shortest_path = path;
                shortest_path_size = temp_path_size;
            }

            path = original_path;
            selectable_parts[i] = true;
        }
    }

    else if(depth == parts.size()) {
        // Last location, adding end to the path
        shortest_path_size = path.back()->GetLocation().Distance(end);
        return shortest_path_size;
    }

    else {
        // Adding parts to the path
        for(uint32_t i = 0; i < parts.size(); i++) {
            if(selectable_parts[i]) {
                selectable_parts[i] = false;
                path.push_back(parts[i]);

                temp_path_size = GetPathLength(start, end, parts, path, selectable_parts, depth + 1) + path[depth - 1]->GetLocation().Distance(parts[i]->GetLocation());
                if(temp_path_size < shortest_path_size) {
                    shortest_path = path;
                    shortest_path_size = temp_path_size;
                }

                path = original_path;
                selectable_parts[i] = true;
            }
        }
    }

    path = shortest_path;
    return shortest_path_size;
}

// Generating the output in the required format
std::string PathFinder::GeneratePathPrint(Order *order, std::string order_description, std::vector<Part*> shortest_path) {
    std::string out_string = "Working on order " + std::to_string(order->GetId()) + " (" + order_description + ")\n";

    std::vector<Product*> products = order->GetProducts();

    uint32_t index = 0;

    for(uint32_t i = 0; i < shortest_path.size(); i++) {
        for(uint32_t j = 0; j < products.size(); j++) {
            uint32_t part_count = products[j]->GetPartCount(shortest_path[i]);
            if(part_count > 0) {
                out_string += std::to_string(++index) + ".\t";
                out_string += "Fetching " + (part_count == 1 ? "part" : std::to_string(part_count) + " parts") + " '" + shortest_path[i]->GetName() + "' ";
                out_string += "for product '" + std::to_string(products[j]->GetId()) + "' ";
                out_string += "at " + shortest_path[i]->GetLocation().ToString() + '\n';
            }
        }
    }
    out_string += std::to_string(++index) + ".\t";
    out_string += "Delivering to destination " + order->GetLocation().ToString() + '\n';

    return out_string;
}

}   // namespace order_optimizer